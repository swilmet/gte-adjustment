#include <gtk/gtk.h>

/* 10k */
#define NUM_LINES 10000

static void
fill_text_buffer (GtkTextBuffer *buffer)
{
  GString *string;
  gint line;
  GtkTextIter start_iter;

  string = g_string_new (NULL);
  for (line = 0; line < NUM_LINES; line++)
    g_string_append_printf (string, "%d\n", line);

  gtk_text_buffer_get_start_iter (buffer, &start_iter);
  gtk_text_buffer_insert (buffer, &start_iter, string->str, string->len);

  g_string_free (string, TRUE);
}

static void
scroll_to_middle (GtkTextView *text_view)
{
  GtkTextBuffer *buffer;
  GtkTextIter iter;

  buffer = gtk_text_view_get_buffer (text_view);
  gtk_text_buffer_get_iter_at_line_offset (buffer,
                                           &iter,
                                           NUM_LINES / 2,
                                           1); /* To better see the cursor. */

  gtk_text_buffer_place_cursor (buffer, &iter);

  gtk_text_view_scroll_to_mark (text_view,
                                gtk_text_buffer_get_insert (buffer),
                                0.0, FALSE, 0.0, 0.0);
}

static GtkWidget *
create_window_content (void)
{
  GtkTextView *text_view;
  GtkWidget *scrolled_window;

  text_view = GTK_TEXT_VIEW (gtk_text_view_new ());
  gtk_text_view_set_monospace (text_view, TRUE);

  fill_text_buffer (gtk_text_view_get_buffer (text_view));
  scroll_to_middle (text_view);

  scrolled_window = gtk_scrolled_window_new (NULL, NULL);

  gtk_container_add (GTK_CONTAINER (scrolled_window), GTK_WIDGET (text_view));
  gtk_widget_show_all (scrolled_window);

  return scrolled_window;
}

int
main (int    argc,
      char **argv)
{
  GtkWidget *window;

  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_maximize (GTK_WINDOW (window));
  gtk_container_add (GTK_CONTAINER (window), create_window_content ());
  gtk_widget_show (window);

  g_signal_connect (window,
                    "destroy",
                    G_CALLBACK (gtk_main_quit),
                    NULL);

  gtk_main ();
  return 0;
}
