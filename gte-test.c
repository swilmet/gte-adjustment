#include "gte-adjustment.h"

static void
test_value_get_set (void)
{
  GteAdjustment *adjustment = gte_adjustment_new ();
  gdouble value;

  /* Check default value */
  value = gte_adjustment_get_value (adjustment);
  g_assert_cmpfloat (value, ==, 0.0);

  /* Set/get */
  gte_adjustment_set_value (adjustment, 7.5);
  value = gte_adjustment_get_value (adjustment);
  g_assert_cmpfloat (value, ==, 7.5);

  g_object_unref (adjustment);
}

typedef struct
{
  gdouble expected_value;
  gint n_notified;
} NotifyData;

static void
reset_notify_data (NotifyData *data,
                   gdouble     expected_value)
{
  data->expected_value = expected_value;
  data->n_notified = 0;
}

static void
notify_value_cb (GteAdjustment *adjustment,
                 GParamSpec    *pspec,
                 gpointer       user_data)
{
  NotifyData *data = user_data;
  gdouble value;

  value = gte_adjustment_get_value (adjustment);
  g_assert_cmpfloat (value, ==, data->expected_value);

  data->n_notified++;
}

static void
test_value_receive_notify (void)
{
  GteAdjustment *adjustment = gte_adjustment_new ();
  NotifyData data;

  reset_notify_data (&data, 0.0);

  g_signal_connect (adjustment,
                    "notify::value",
                    G_CALLBACK (notify_value_cb),
                    &data);

  gte_adjustment_set_value (adjustment, 0.0);
  g_assert_cmpint (data.n_notified, ==, 0);

  reset_notify_data (&data, 2.5);
  gte_adjustment_set_value (adjustment, 2.5);
  g_assert_cmpint (data.n_notified, ==, 1);

  reset_notify_data (&data, 2.5);
  gte_adjustment_set_value (adjustment, 2.5);
  g_assert_cmpint (data.n_notified, ==, 0);

  reset_notify_data (&data, 2.5);
  g_object_set (adjustment,
                "value", 2.5,
                NULL);
  g_assert_cmpint (data.n_notified, ==, 0);

  g_object_unref (adjustment);
}

static void
test_set_value_and_lower (void)
{
  GteAdjustment *adjustment = gte_adjustment_new ();

  /* Set an initial :lower bound */

  gte_adjustment_set_lower (adjustment, 10.0);
  // The value has been clamped.
  g_assert_cmpfloat (gte_adjustment_get_value (adjustment), ==, 10.0);

  /* Set both properties at once, in a certain order */

  g_object_set (adjustment,
                "lower", 0.0, // first set lower
                "value", 5.0, // then value
                NULL);

  g_assert_cmpfloat (gte_adjustment_get_value (adjustment), ==, 5.0);
  g_assert_cmpfloat (gte_adjustment_get_lower (adjustment), ==, 0.0);
  // ===> OK

  /* Start again, but now set both properties at once in the opposite order */

  gte_adjustment_set_lower (adjustment, 10.0);

  g_object_set (adjustment,
                "value", 5.0, // first set value
                "lower", 0.0, // then lower
                NULL);

  g_assert_cmpfloat (gte_adjustment_get_value (adjustment), ==, 5.0);
  g_assert_cmpfloat (gte_adjustment_get_lower (adjustment), ==, 0.0);
  // ===> FAILS, value is still 10.0.

  g_object_unref (adjustment);
}

int
main (int    argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/adjustment/test_value_get_set",
                   test_value_get_set);
  g_test_add_func ("/adjustment/test_value_receive_notify",
                   test_value_receive_notify);
  g_test_add_func ("/adjustment/test_set_value_and_lower",
                   test_set_value_and_lower);

  return g_test_run ();
}
