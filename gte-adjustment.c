#include "gte-adjustment.h"

#define DEFAULT_VALUE_FOR_DOUBLE_PROPERTY 0.0

/* Still very simple, but can become more complicated when adding more
 * properties. Have the infra to make the code easier to understand.
 */
typedef struct
{
  gdouble lower;
} ClampInfo;

struct _GteAdjustmentPrivate
{
  /* Invariant: @value respects the @clamp_info. */
  gdouble value;
  ClampInfo clamp_info;
};

enum
{
  PROP_0,
  PROP_VALUE,
  PROP_LOWER,
  N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

G_DEFINE_TYPE_WITH_PRIVATE (GteAdjustment, gte_adjustment, G_TYPE_OBJECT)

static gboolean
clamp_value (ClampInfo *clamp_info,
             gdouble   *value)
{
  gboolean changed = FALSE;

  if (*value < clamp_info->lower)
    {
      *value = clamp_info->lower;
      changed = TRUE;
    }

  return changed;
}

static void
gte_adjustment_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  GteAdjustment *adjustment = GTE_ADJUSTMENT (object);

  switch (prop_id)
    {
      case PROP_VALUE:
        g_value_set_double (value, gte_adjustment_get_value (adjustment));
        break;

      case PROP_LOWER:
        g_value_set_double (value, gte_adjustment_get_lower (adjustment));
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
gte_adjustment_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  GteAdjustment *adjustment = GTE_ADJUSTMENT (object);

  switch (prop_id)
    {
      case PROP_VALUE:
        gte_adjustment_set_value (adjustment, g_value_get_double (value));
        break;

      case PROP_LOWER:
        gte_adjustment_set_lower (adjustment, g_value_get_double (value));
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static GParamSpec *
param_spec_double_simple (const gchar *name)
{
  return g_param_spec_double (name, name, "",
                              -G_MAXDOUBLE, G_MAXDOUBLE,
                              DEFAULT_VALUE_FOR_DOUBLE_PROPERTY,
                              G_PARAM_READWRITE |
                              G_PARAM_STATIC_STRINGS |
                              G_PARAM_EXPLICIT_NOTIFY);
}

static void
gte_adjustment_class_init (GteAdjustmentClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = gte_adjustment_get_property;
  object_class->set_property = gte_adjustment_set_property;

  properties[PROP_VALUE] = param_spec_double_simple ("value");
  properties[PROP_LOWER] = param_spec_double_simple ("lower");
  g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

static void
clamp_info_init (ClampInfo *clamp_info)
{
  clamp_info->lower = DEFAULT_VALUE_FOR_DOUBLE_PROPERTY;
}

static void
gte_adjustment_init (GteAdjustment *adjustment)
{
  GteAdjustmentPrivate *priv;

  adjustment->priv = gte_adjustment_get_instance_private (adjustment);
  priv = adjustment->priv;

  /* Since G_PARAM_CONSTRUCT is not used, it's better to initialize the property
   * values here in init(), with a macroconstant.
   */
  priv->value = DEFAULT_VALUE_FOR_DOUBLE_PROPERTY;
  clamp_info_init (&priv->clamp_info);
}

GteAdjustment *
gte_adjustment_new (void)
{
  return g_object_new (GTE_TYPE_ADJUSTMENT, NULL);
}

gdouble
gte_adjustment_get_value (GteAdjustment *adjustment)
{
  g_return_val_if_fail (GTE_IS_ADJUSTMENT (adjustment), DEFAULT_VALUE_FOR_DOUBLE_PROPERTY);

  return adjustment->priv->value;
}

void
gte_adjustment_set_value (GteAdjustment *adjustment,
                          gdouble        value)
{
  GteAdjustmentPrivate *priv;
  gdouble clamped_value;

  g_return_if_fail (GTE_IS_ADJUSTMENT (adjustment));

  priv = adjustment->priv;

  clamped_value = value;
  clamp_value (&priv->clamp_info, &clamped_value);

  if (priv->value != clamped_value)
    {
      priv->value = clamped_value;
      g_object_notify_by_pspec (G_OBJECT (adjustment), properties[PROP_VALUE]);
    }
}

gdouble
gte_adjustment_get_lower (GteAdjustment *adjustment)
{
  g_return_val_if_fail (GTE_IS_ADJUSTMENT (adjustment), DEFAULT_VALUE_FOR_DOUBLE_PROPERTY);

  return adjustment->priv->clamp_info.lower;
}

void
gte_adjustment_set_lower (GteAdjustment *adjustment,
                          gdouble        lower)
{
  GteAdjustmentPrivate *priv;
  gboolean lower_changed = FALSE;
  gboolean value_changed = FALSE;

  g_return_if_fail (GTE_IS_ADJUSTMENT (adjustment));

  priv = adjustment->priv;

  if (priv->clamp_info.lower != lower)
    {
      priv->clamp_info.lower = lower;
      lower_changed = TRUE;

      value_changed = clamp_value (&priv->clamp_info, &priv->value);
    }

  /* Emit notify signals when all class invariants are respected. */
  if (lower_changed)
    g_object_notify_by_pspec (G_OBJECT (adjustment), properties[PROP_LOWER]);
  if (value_changed)
    g_object_notify_by_pspec (G_OBJECT (adjustment), properties[PROP_VALUE]);
}
