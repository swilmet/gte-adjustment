#include <gtk/gtk.h>

static void
test_set_value_and_lower (void)
{
  GtkAdjustment *adjustment = gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  g_object_ref_sink (adjustment);

  /* Set an initial :lower bound */

  g_assert_cmpfloat (gtk_adjustment_get_value (adjustment), ==, 0.0);
  gtk_adjustment_set_lower (adjustment, 10.0);
  // The value has been clamped.
  g_assert_cmpfloat (gtk_adjustment_get_value (adjustment), ==, 10.0);
  // ===> FAILS, value is still 0.0.

  /* Set both properties at once, in a certain order */

  g_object_set (adjustment,
                "lower", 0.0, // first set lower
                "value", 5.0, // then value
                NULL);

  g_assert_cmpfloat (gtk_adjustment_get_value (adjustment), ==, 5.0);
  g_assert_cmpfloat (gtk_adjustment_get_lower (adjustment), ==, 0.0);
  // ===> OK

  /* Start again, but now set both properties at once in the opposite order */

  gtk_adjustment_set_lower (adjustment, 10.0);

  g_object_set (adjustment,
                "value", 5.0, // first set value
                "lower", 0.0, // then lower
                NULL);

  g_assert_cmpfloat (gtk_adjustment_get_value (adjustment), ==, 5.0);
  g_assert_cmpfloat (gtk_adjustment_get_lower (adjustment), ==, 0.0);
  // ===> FAILS, value is still 10.0.

  g_object_unref (adjustment);
}

int
main (int    argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/adjustment/test_set_value_and_lower",
                   test_set_value_and_lower);

  return g_test_run ();
}
