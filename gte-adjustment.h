#ifndef GTE_ADJUSTMENT_H
#define GTE_ADJUSTMENT_H

#include <glib-object.h>

G_BEGIN_DECLS

#define GTE_TYPE_ADJUSTMENT             (gte_adjustment_get_type ())
#define GTE_ADJUSTMENT(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTE_TYPE_ADJUSTMENT, GteAdjustment))
#define GTE_ADJUSTMENT_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTE_TYPE_ADJUSTMENT, GteAdjustmentClass))
#define GTE_IS_ADJUSTMENT(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTE_TYPE_ADJUSTMENT))
#define GTE_IS_ADJUSTMENT_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTE_TYPE_ADJUSTMENT))
#define GTE_ADJUSTMENT_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTE_TYPE_ADJUSTMENT, GteAdjustmentClass))

typedef struct _GteAdjustment         GteAdjustment;
typedef struct _GteAdjustmentClass    GteAdjustmentClass;
typedef struct _GteAdjustmentPrivate  GteAdjustmentPrivate;

struct _GteAdjustment
{
  GObject parent;

  GteAdjustmentPrivate *priv;
};

struct _GteAdjustmentClass
{
  GObjectClass parent_class;
};

GType           gte_adjustment_get_type       (void);

GteAdjustment * gte_adjustment_new            (void);

gdouble         gte_adjustment_get_value      (GteAdjustment *adjustment);

void            gte_adjustment_set_value      (GteAdjustment *adjustment,
                                               gdouble        value);

gdouble         gte_adjustment_get_lower      (GteAdjustment *adjustment);

void            gte_adjustment_set_lower      (GteAdjustment *adjustment,
                                               gdouble        lower);

G_END_DECLS

#endif /* GTE_ADJUSTMENT_H */
