GteAdjustment notes
===================

Gte ("te" for test).

A new mini Adjustment class.

Ideas such as:
- Ensure that class invariants are respected at all times.
- Store the original value before being clamped.

With **Gtk**Adjustment, it's the user of the class that controls the values.

Step 1 - a single property, :value
----------------------------------

Easy.

Step 2 - adding the :lower property
-----------------------------------

### So, :lower needs to be applied to :value, but where?

We probably want to keep the original :value set, even if out of range.

Then apply :lower to :value when wanting the "applied" / clamped value.

Why keeping the original :value?
1. Because it can *become* in range afterwards, when setting a different :lower.
   But the user of the class better knows whether re-clamping the value makes
   sense or not.
2. When setting several properties at once, if the original value is not stored
   (only the clamped value is), it would depend on the order that the properties
   are set.
3. (less important) When we set() a value, it can be surprising to get() a
   different value.

### Currently with GTK

- It doesn't keep the original :value set (but it can be stored externally of
  course).
- `gtk_adjustment_set_value()` clamps the value before storing it.
- The user of the class needs to pay attention when setting several properties
  at once.
