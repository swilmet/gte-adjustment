cmake_minimum_required (VERSION 3.20)
project (gte
  VERSION 0.1.0
  LANGUAGES C)

include (cmake/gsw.cmake)
GswInitBasic ()

pkg_check_modules (DEPS REQUIRED gobject-2.0)
pkg_check_modules (GTK_DEPS REQUIRED gtk+-3.0)

set (sources
  gte-adjustment.c
  gte-adjustment.h
  gte-test.c)

add_definitions ("-DG_LOG_DOMAIN=\"${PROJECT_NAME}\"")

GswCompilerFlags ()
GswAddExecutable (DEPS "${PROJECT_NAME}" "${sources}")
GswAddExecutable (GTK_DEPS gtk-adjustment-test gtk-adjustment-test.c)

add_subdirectory (gtv-scroll)
